<?php

defined('BOOTSTRAP') or die('Access denied');

/**
 * Get an array of all employees or by ID from table sd_hr_management_employees
 * @param int|null $employee_id  Employee identifier
 * @return array structured data
 */
function fn_get_employees($employee_id = null) {
    if (!isset($employee_id)) {
        return db_get_array("SELECT * FROM ?:sd_hr_management_employees");
    } else {
        return db_get_array("SELECT * FROM ?:sd_hr_management_employees WHERE id = ?i", $employee_id);
    }
}
