<?php

$schema['central']['website']['items']['sd_hr_management'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'position' => 0,
    'href' => 'sd_hr_management.manage'
);

return $schema;
