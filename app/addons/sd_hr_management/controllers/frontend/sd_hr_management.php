<?php

defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($mode === 'details') {
        fn_add_breadcrumb(__("detailed_description"));
        $employees = fn_get_employees($_REQUEST['employee']['id']);
        Tygh::$app['view']->assign('employees', $employees);
    }
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'view') {
    fn_add_breadcrumb(__("sd_hr_management.employees_list"));
    $employees = fn_get_employees();
    Tygh::$app['view']->assign('employees', $employees);
}
