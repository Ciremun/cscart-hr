<?php

defined('BOOTSTRAP') or die('Access denied');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($mode === 'update') {
        $id = array_shift($_REQUEST['employee']);
        $fields = $_REQUEST['employee'];
        db_query("UPDATE ?:sd_hr_management_employees SET ?u WHERE id = ?i", $fields, $id);
        return array(CONTROLLER_STATUS_OK, 'sd_hr_management.manage');
    }
    if ($mode === 'add') {
        db_query("INSERT INTO ?:sd_hr_management_employees ?e", $_REQUEST['employee']);
        return array(CONTROLLER_STATUS_OK, 'sd_hr_management.manage');
    }
    if ($mode === 'delete') {
        db_query("DELETE FROM ?:sd_hr_management_employees WHERE id = ?i", $_REQUEST['id']);
        return array(CONTROLLER_STATUS_OK, 'sd_hr_management.manage');
    }
    return [CONTROLLER_STATUS_OK];
}

if ($mode === 'manage') {
    $employees = fn_get_employees();
    Tygh::$app['view']->assign('employees', $employees);
}

if ($mode === 'update') {
    $employees = fn_get_employees($_REQUEST['id']);
    Tygh::$app['view']->assign('employees', $employees);
}
