{capture name="mainbox"}

<table style="width: 100%" class="table table-tree table-middle table--relative table-nobg table-responsive">
    <tr>
        <th>{__("position_short")}</th>
        <th>{__("first_name")}</th>
        <th>{__("last_name")}</th>
        <th>{__("sd_hr_management.function")}</th>
        <th>{__("email")}</th>
        <th>{__("sd_hr_management.description")}</th>
        <th>{__("status")}</th>
        <th>{__("actions")}</th>
    </tr>
    {foreach from=$employees item=employee}
    {$form_name="employee_`$employee.id`_form"}
    <form action="{""|fn_url}" method="post" name={$form_name} id={$form_name}>
        <tr>
            <input type="hidden" name="employee[id]" value="{$employee.id}"/>
            <td><input type="text" style="width: 150px" name="employee[pos]" value="{$employee.pos}" placeholder="{__(position_short)}" style="width: 40px; text-align: center;"></td>
            <td><input type="text" style="width: 150px" name="employee[first_name]" value="{$employee.first_name}" placeholder="{__(first_name)}"></td>
            <td><input type="text" style="width: 150px" name="employee[last_name]" value="{$employee.last_name}" placeholder="{__(last_name)}"></td>
            <td><input type="text" style="width: 150px" name="employee[title]" value="{$employee.title}" placeholder="{__("sd_hr_management.function")}"></td>
            <td><input type="text" style="width: 150px" name="employee[email]" value="{$employee.email}" placeholder="{__("email")}"></td>
            <td><textarea style="width: 150px" name="employee[description]" placeholder="{__("sd_hr_management.description")}">{$employee.description}</textarea></td>
            <td>
                <select style="width: 80px" name="employee[status]">
                    {if $employee.status == "E"}
                        <option value="E" selected>{__("enable")}</option>
                        <option value="D">{__("disable")}</option>
                    {else}
                        <option value="E">{__("enable")}</option>
                        <option value="D" selected>{__("disable")}</option>
                    {/if}
                </select>
            </td>
            <td style="width: 10%">
                {btn type="list" text=__("update") dispatch="dispatch[sd_hr_management.update]" form=$form_name}
                {btn type="list" text=__("delete") class="cm-confirm" href="sd_hr_management.delete?id=`$employee.id`" method="POST"}
            </td>
        </tr>
    </form>
    {/foreach}
</table>

{/capture}

{include file="common/mainbox.tpl" title=__("sd_hr_management") content=$smarty.capture.mainbox}