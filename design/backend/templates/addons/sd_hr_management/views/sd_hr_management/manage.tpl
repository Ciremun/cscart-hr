{capture name="mainbox"}

<table style="width: 100%" class="table table-tree table-middle table--relative table-nobg table-responsive">
    <tr>
        <th>{__("position_short")}</th>
        <th>{__("first_name")}</th>
        <th>{__("last_name")}</th>
        <th>{__("sd_hr_management.function")}</th>
        <th>{__("email")}</th>
        <th>{__("sd_hr_management.description")}</th>
        <th>{__("status")}</th>
        <th>{__("actions")}</th>
    </tr>
    <tr>
        <form action="{""|fn_url}" method="post" name="employee_add_form" id="employee_add_form">
            <td><input style="width: 40px; text-align: center;" type="text" name="employee[pos]" placeholder="{__(position_short)}"></td>
            <td><input type="text" style="width: 150px" name="employee[first_name]" placeholder="{__(first_name)}"></td>
            <td><input type="text" style="width: 150px" name="employee[last_name]" placeholder="{__(last_name)}"></td>
            <td><input type="text" style="width: 150px" name="employee[title]" placeholder="{__("sd_hr_management.function")}"></td>
            <td><input type="text" style="width: 150px" name="employee[email]" placeholder="{__("email")}"></td>
            <td><textarea type="text" style="width: 150px" name="employee[description]" placeholder="{__("sd_hr_management.description")}"></textarea></td>
            <td>
                <select style="width: 80px" name="employee[status]">
                    <option value="E">{__("enable")}</option>
                    <option value="D">{__("disable")}</option>
                </select>
            </td>
            <td style="width: 10%">{btn type="list" text=__("add") dispatch="dispatch[sd_hr_management.add]" form="employee_add_form"}</td>
        </form>
    </tr>
</table>

<br>
<br>

<table style="width: 100%" class="table table-tree table-middle table--relative table-nobg table-responsive">
    <tr>
        <th>{__("position_short")}</th>
        <th>{__("first_name_and_last_name")}</th>
        <th>{__("status")}</th>
        <th>{__("actions")}</th>
    </tr>
    {foreach from=$employees item=employee}
        <tr>
            <input type="hidden" name="employee[id]" value="{$employee.id}">
            <td><input type="text" name="employee[pos]" value="{$employee.pos}" style="width: 40px; text-align: center;" readonly></td>
            <td>{$employee.first_name} {$employee.last_name}</td>
            <td>{($employee.status == "E") ? __("enable") : __("disable")}</td>
            <td style="width: 10%">{btn type="list" text=__("edit") class="cm-confirm" href="sd_hr_management.update?id=`$employee.id`" method="GET"}</td>
        </tr>
    {/foreach}
</table>

{/capture}

{include file="common/mainbox.tpl" title=__("sd_hr_management") content=$smarty.capture.mainbox}