<table style="width: 100%" class="table table-tree table-middle table--relative table-nobg table-responsive">
    <tr style="text-align: left">
        <th>{__("position_short")}</th>
        <th>{__("first_name_and_last_name")}</th>
        <th>{__("status")}</th>
    </tr>
    {foreach from=$employees item=employee}
    {$form_name="employee_`$employee.id`_form"}
    <form action="{""|fn_url}" method="post" name={$form_name} id={$form_name}>
        <tr>
            <input type="hidden" name="employee[id]" value="{$employee.id}">
            <td><input type="text" value="{$employee.pos}" style="width: 40px; text-align: center;" readonly></td>
            <td><button type="submit" name="dispatch[sd_hr_management.details]">{$employee.first_name} {$employee.last_name}</button></td>
            <td>{($employee.status == "E") ? __("enable") : __("disable")}</td>
        </tr>
    </form>
    {/foreach}
</table>

{capture name="mainbox_title"}{__("sd_hr_management.employees_list")}{/capture}