<table style="width: 100%" class="table table-tree table-middle table--relative table-nobg table-responsive">
    <tr>
        <th>{__("position_short")}</th>
        <th>{__("first_name")}</th>
        <th>{__("last_name")}</th>
        <th>{__("sd_hr_management.function")}</th>
        <th>{__("email")}</th>
        <th>{__("sd_hr_management.description")}</th>
        <th>{__("status")}</th>
    </tr>
    {foreach from=$employees item=employee}
        <tr style="text-align: center">
            <td>{$employee.pos}</td>
            <td>{$employee.first_name}</td>
            <td>{$employee.last_name}</td>
            <td>{$employee.title}</td>
            <td>{$employee.email}</td>
            <td>{$employee.description}</td>
            <td>{($employee.status == "E") ? __("enable") : __("disable")}</td>
        </tr>
    {/foreach}
</table>

{capture name="mainbox_title"}{__("detailed_description")}{/capture}